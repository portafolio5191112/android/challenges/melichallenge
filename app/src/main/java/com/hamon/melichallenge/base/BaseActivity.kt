package com.hamon.melichallenge.base

import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import timber.log.Timber

open class BaseActivity : AppCompatActivity(), ILogBase, IViewBase {

    //toast functions
    override fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun toastLong(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun blockUI() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    override fun unblockUI() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    //logs functions
    override fun logDebug(message: String) {
        Timber.d(message)
    }

    override fun logDebug(tag: String, message: String) {
        Timber.tag(tag).d(message)
    }

    override fun logInfo(message: String) {
        Timber.i(message)
    }

    override fun logInfo(tag: String, message: String) {
        Timber.tag(tag).i(message)
    }

    override fun logWarning(message: String) {
        Timber.w(message)
    }

    override fun logWarning(tag: String, message: String) {
        Timber.tag(tag).w(message)
    }

    override fun logError(message: String) {
        Timber.e(message)
    }

    override fun logError(tag: String, message: String) {
        Timber.tag(tag).e(message)
    }

}