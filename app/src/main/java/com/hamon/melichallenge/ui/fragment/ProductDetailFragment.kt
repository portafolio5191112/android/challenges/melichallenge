package com.hamon.melichallenge.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import coil.load
import com.hamon.melichallenge.R
import com.hamon.melichallenge.actions.MeliProductSelectedActions
import com.hamon.melichallenge.actions.NetworkActions
import com.hamon.melichallenge.base.BaseFragment
import com.hamon.melichallenge.databinding.FragmentProductDetailBinding
import com.hamon.melichallenge.ui.adapter.PictureAdapter
import com.hamon.melichallenge.viewmodel.MainViewModel
import com.hamon.provider.model.ItemMeli
import com.hamon.provider.model.Picture
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ProductDetailFragment : BaseFragment() {

    private val binding: FragmentProductDetailBinding by lazy {
        FragmentProductDetailBinding.inflate(LayoutInflater.from(context), null, false)
    }
    private val viewModel: MainViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let { viewModel.isNetworkConnected(it) }
        setupObservables()
    }

    private fun productInfo() {
        viewModel.apply {
            restartNetworkEvent()
            getProductInfo()
        }
    }

    private fun setupObservables() {
        viewModel.apply {
            getItemActions.observe(viewLifecycleOwner) { event ->
                when (event) {
                    is MeliProductSelectedActions.HasData -> setupUI(event.product)
                    is MeliProductSelectedActions.HasError -> hasAnError(event.message)
                }
            }
            networkActions.observe(viewLifecycleOwner) { event ->
                when (event) {
                    is NetworkActions.NetworkOK -> productInfo()
                    is NetworkActions.NetworkError -> navigateUp()
                }
            }
        }
    }

    private fun navigateUp() {
        findNavController().navigateUp()
        hideProgressBar()
    }

    private fun hasAnError(message: String) {
        hideProgressBar()
        showSnackBarError(message)
    }

    private fun setupUI(product: ItemMeli) {
        binding.apply {
            tvProductName.text = product.title
            tvProductState.text = getString(
                R.string.product_state,
                getProductState(product.condition),
            )
            tvProductSold.text = getString(R.string.product_sold, product.soldQuantity)
            tvProductAvaliable.text =
                getString(R.string.product_available, product.availableQuantity)
            tvShipping.text =
                getString(R.string.free_shipping, decideYesOrNot(product.shipping.freeShipping))
            tvCredit.text =
                getString(
                    R.string.accept_mercado_pago,
                    decideYesOrNot(product.acceptsMercadopago)
                )
            tvProductPrice.text = getString(R.string.price_product, product.price.toString())
        }
        setupProductSlider(product.pictures.toMutableList())
        showWarranty(product.warranty)
        hideProgressBar()
        showProductContainer()
    }

    private fun showWarranty(warranty: String){
        if (warranty.isNotEmpty() || warranty != "null"){
            binding.apply {
                tvWarranty.text = warranty
                cWarranty.visibility = View.VISIBLE
            }
        }else{
            binding.cWarranty.visibility = View.GONE
        }
    }

    private fun showProductContainer(){
        binding.cProduct.visibility = View.VISIBLE
    }

    private fun setupProductSlider(listOfProducts: MutableList<Picture>) {
        binding.apply {
            vpPictures.adapter = PictureAdapter(this@ProductDetailFragment, listOfProducts)
            vpIndicator.setViewPager(vpPictures)
        }
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        findNavController().navigateUp()
    }

    companion object {
        const val STATE_NEW = "new"
    }

    private fun decideYesOrNot(isFreeShipping: Boolean): String {
        return if (isFreeShipping)
            getString(R.string.yes)
        else getString(R.string.no)
    }

    private fun getProductState(state: String): String {
        return if (state == STATE_NEW)
            getString(R.string.product_new)
        else getString(R.string.product_used)
    }

}