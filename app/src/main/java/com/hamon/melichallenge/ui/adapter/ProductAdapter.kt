package com.hamon.melichallenge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hamon.melichallenge.databinding.LayoutItemMeliProductBinding
import com.hamon.melichallenge.databinding.LayoutPagingLoadingBinding
import com.hamon.melichallenge.ui.viewholder.MeliProductViewHolder
import com.hamon.melichallenge.ui.viewholder.ProgressViewHolder
import com.hamon.provider.model.MeliProduct

class ProductAdapter(
    private val productList: MutableList<MeliProduct> = mutableListOf(),
    private val onTapProduct: (product: MeliProduct) -> Unit,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoadingProduct: Boolean = false

    fun addProducts(list: MutableList<MeliProduct>) {
        productList.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        productList.clear()
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoadingProduct = true
        productList.add(MeliProduct())
        notifyItemInserted(productList.size - 1)
    }

    fun removeLoading() {
        val position = productList.size -1
        if (position > 0) productList.removeAt(position)
        notifyItemRemoved(position)
        isLoadingProduct = false
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MeliProductViewHolder -> holder.bind(productList[position], onTapProduct)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NORMAL_VIEW) {
            MeliProductViewHolder(
                LayoutItemMeliProductBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            ProgressViewHolder(
                LayoutPagingLoadingBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = productList.size

    override fun getItemViewType(position: Int): Int {
        return if (isLoadingProduct) {
            if (position == (productList.size - 1)) LOADING_VIEW else NORMAL_VIEW
        } else {
            NORMAL_VIEW
        }
    }

    companion object {
        const val NORMAL_VIEW = 0
        const val LOADING_VIEW = 1
    }

}