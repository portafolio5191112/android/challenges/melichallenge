package com.hamon.melichallenge.ui.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.hamon.melichallenge.ui.fragment.ProductImage
import com.hamon.provider.model.Picture

class PictureAdapter(fragment: Fragment, private val listOfImage: MutableList<Picture> = mutableListOf()) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = listOfImage.size

    override fun createFragment(position: Int): Fragment =
        ProductImage.newInstance(listOfImage[position].secureUrl)
}