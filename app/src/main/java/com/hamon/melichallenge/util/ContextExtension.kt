package com.hamon.melichallenge.util

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.showKeyboard(){
    val inputMethodManager: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun Context.addKeyboardVisibilityListener(
    rootLayout: View,
    onKeyboardVisibilityListener: OnKeyboardVisibilityListener
) {
    rootLayout.viewTreeObserver.addOnGlobalLayoutListener {
        val r = Rect()
        rootLayout.getWindowVisibleDisplayFrame(r)
        val screenHeight: Int = rootLayout.rootView.height

        // r.bottom is the position above soft keypad or device button.
        // if keypad is shown, the r.bottom is smaller than that before.
        val keypadHeight: Int = screenHeight - r.bottom
        val isVisible =
            keypadHeight > screenHeight * 0.15 // 0.15 ratio is perhaps enough to determine keypad height.
        onKeyboardVisibilityListener.onVisibilityChange(isVisible)
    }
}

interface OnKeyboardVisibilityListener {
    fun onVisibilityChange(isVisible: Boolean)
}