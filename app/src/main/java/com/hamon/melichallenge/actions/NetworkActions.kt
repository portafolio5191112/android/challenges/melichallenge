package com.hamon.melichallenge.actions

sealed class NetworkActions {
    object Init: NetworkActions()
    object NetworkOK: NetworkActions()
    object NetworkError: NetworkActions()
}