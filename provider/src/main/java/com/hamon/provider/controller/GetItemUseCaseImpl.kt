package com.hamon.provider.controller

import com.hamon.provider.actions.APIActions
import com.hamon.provider.repository.RemoteDataSource

class GetItemUseCaseImpl(private val remoteDataSource: RemoteDataSource) : GetItemUseCase {
    override suspend fun invoke(productId: String): APIActions =
        remoteDataSource.getProductInfo(productId)
}

interface GetItemUseCase {
    suspend fun invoke(productId: String): APIActions
}