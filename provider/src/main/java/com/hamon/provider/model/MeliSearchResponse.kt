package com.hamon.provider.model

import com.google.gson.annotations.SerializedName

data class MeliSearchResponse(
    @SerializedName("results") val results: MutableList<MeliProduct> = mutableListOf(),
)